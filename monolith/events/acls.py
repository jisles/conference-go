import json
import requests

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from pprint import pprint

# use the Pexels API
def get_photo(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
    }
    url = 'https://api.pexels.com/v1/search'
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

# use the open weather API
def get_weather_data(city, state):
    url = 'http://api.openweathermap.org/geo/1.0/direct'
    params = {
        "q": f"{city}, {state}, 840",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
        }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        latlong = {"lat": content[0]['lat'], "lon": content[0]['lon']}
        pprint(latlong)
    except (KeyError, IndexError):
        return None

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": latlong["lat"],
        "lon": latlong["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    # print(content["main"]["temp"])
    # print(content["weather"][0]["description"])
    try:
        main_temp = content["main"]["temp"]
        description = content["weather"][0]["description"]
        print(main_temp, description)
        return {"main_temp": main_temp, "description": description}
    except (KeyError, IndexError):
        return None
