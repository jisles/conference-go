from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
import pika, sys, os


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


from attendees.models import AccountVO


def accountvo_object(ch, method, properties, body):
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = str(updated_string)
    try:
        if is_active:
            AccountVO.objects.update_or_create(
                email=email,
                defaults={
                    "first_name": first_name,
                    "last_name": last_name,
                    "is_active": is_active,
                    "updated": updated,
                }
            )
        else:
            AccountVO.objects.filter(email=email).delete()
    except ObjectDoesNotExist:
        pass

# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop


while True:
    #   try
    try:
        # create the pika connection parameters
        parameters = pika.ConnectionParameters(host='rabbitmq')
    #   create a blocking connection with the parameters
        connection = pika.BlockingConnection(parameters)
    #   open a channel
        channel = connection.channel()
    #   declare a fanout exchange named "account_info"
        channel.exchange_declare(
            exchange='account_info',
            exchange_type='fanout',
            )
    #   declare a randomly-named queue
        result = channel.queue_declare(queue='', exclusive=True)
    #   get the queue name of the randomly-named queue
        queue_name = result.method.queue
    #   bind the queue to the "account_info" exchange
        channel.queue_bind(exchange="account_info", queue=queue_name)
    #   do a basic_consume for the queue name that calls
        #   function above

        def callback(ch, method, properties, body):
            print(" [x] %r" % body.decode())  # i don't know, man

        print(' [*] Waiting for logs. To exit press CTRL+C')
        channel.basic_consume(
            queue=queue_name, on_message_callback=callback, auto_ack=True)
    #   tell the channel to start consuming
        channel.start_consuming()
#   except AMQPConnectionError
    except AMQPConnectionError:
        # print that it could not connect to RabbitMQ
        print("could not connect to rabbitmq")
    #   have it sleep for a couple of seconds
        time.sleep(2.0)
